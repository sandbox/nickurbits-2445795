(function() {
  function ctrlMain($scope) {
    $scope.items = [{
        nameFirst: 'Galileo',
        nameLast: 'Galilei',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Neil deGrasse',
        nameLast: 'Tyson',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Brian',
        nameLast: 'Cox',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Carl',
        nameLast: 'Sagan',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Michael',
        nameLast: 'Faraday',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Albert',
        nameLast: 'Einstein',
        age: Math.floor(Math.random() * 100)
      },
      {
        nameFirst: 'Nikola',
        nameLast: 'Tesla',
        age: Math.floor(Math.random() * 100)
      }];

    $scope.displayName = function(item) {
      return item.nameFirst + ' ' + item.nameLast;
    }
  }

  angular.module('ngBlocksExampleApp', [])
    .controller('ctrlMain', ctrlMain);
})();
