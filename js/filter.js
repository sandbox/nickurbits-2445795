/**
 * Filter which converts a string into a safe CSS class name.
 */
function filterCssClass() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return input.replace(/[^a-zA-Z_\-]/g, '').toLowerCase();
  };
}

/**
 * Filter which formats a date object as time ago.
 */
function filterFormatDateTimeAgo() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return moment(input).fromNow();
  };
}

/**
 * Filter which formats a date object as long-form human readable datestamp.
 */
function filterFormatDateTimeLong() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return moment(input).format('Do MMMM YYYY, h:mm A');
  };
}

/**
 * Filter which formats a date object as medium-form human readable datestamp.
 */
function filterFormatDateTimeMedium() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return moment(input).format('D/M/YYYY h:mm A');
  };
}

/**
 * Filter which formats a date object as a date string, omitting the time.
 */
function filterFormatDateLong() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return moment(input).format('Do MMMM YYYY');
  };
}

/**
 * Filter which formats a date object as a ISO date.
 */
function filterFormatDateISO() {
  return function(input) {
    if (typeof(input) == 'undefined') {
      return input;
    }
    return moment(input).format('YYYY-MM-DD');
  };
}

angular.module('ngBlocksFilters', [])
  .filter('cssClass', filterCssClass)
  .filter('formatDateTimeAgo', filterFormatDateTimeAgo)
  .filter('formatDateTimeLong', filterFormatDateTimeLong)
  .filter('formatDateTimeMedium', filterFormatDateTimeMedium)
  .filter('formatDateLong', filterFormatDateLong)
  .filter('formatDateISO', filterFormatDateISO);
