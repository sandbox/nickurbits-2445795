// Overriding the modal window template to add an exit button.
angular.module("template/modal/window.html", []).run(["$templateCache", function($templateCache) {
  var template = [
    '<div tabindex="-1" class="modal fade {{ windowClass }}" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">',
    '  <div class="modal-dialog">',
    '    <div title="Close this popup" class="exit" ng-click="close($event)">X</div>',
    '    <div class="modal-content" ng-transclude></div>',
    '  </div>',
    '</div>'
  ].join("\n");
  $templateCache.put("template/modal/window.html", template);
}]);
