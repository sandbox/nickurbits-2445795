/**
 * Angular's default behavior is to only initialise the first ng-app directive.
 *
 * We take over this functionality on behalf of angular so that we can have
 * as many apps on a single page as we need.
 */
Drupal.behaviors.ngBlocksInit = {
  attach: function(context, settings) {
    jQuery('[ng-blocks-app]').not('.ng-scope').once('ngBlocksInit', this.init);
  },
  init: function(key, element) {
    var appName = element.getAttribute('ng-blocks-app');
    angular.bootstrap(element, [appName]);
  }
}
