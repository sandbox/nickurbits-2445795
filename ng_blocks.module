<?php

/**
 * @file
 * Utility module which allows other modules to expose AngularJS apps as blocks.
 */

/**
 * Returns an array of information about ngBlocks's angular apps.
 */
function ng_blocks_app_info() {
  $apps = module_invoke_all('ng_blocks_app_info');
  foreach ($apps as $app_name => &$info) {
    if (!isset($info['app_name'])) {
      $info['app_name'] = $app_name;
    }

    $info += array(
      'label' => '',
      'block_subject' => '',
      'block_cache' => DRUPAL_CACHE_GLOBAL,
    );

    // The callback element is required.
    $required_keys = array('callback', 'js');
    foreach ($required_keys as $key) {
      if (!isset($info[$key])) {
        unset($apps[$app_name]);
        continue 2;
      }
    }

  }
  return $apps;
}

/**
 * Returns single app info array. FALSE if app does not exist.
 */
function ng_blocks_app_get($app_name) {
  $apps = ng_blocks_app_info();
  return (isset($apps[$app_name])) ? $apps[$app_name] : FALSE;
}

/**
 * Implements hook_block_info().
 */
function ng_blocks_block_info() {
  $blocks = array();
  foreach (ng_blocks_app_info() as $app) {
    $blocks[$app['app_name']] = array(
      'info' => 'Angular Block: ' . t($app['label']),
      'cache' => $app['block_cache'],
    );
  }
  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function ng_blocks_block_view($app_name) {
  $block = array();

  // Exit early if we're on an admin page.
  if (path_is_admin(current_path())) {
    return $block;
  }

  if ($app = ng_blocks_app_get($app_name)) {
    $min = _ng_blocks_minified();

    $block['subject'] = $app['block_subject'];

    $render = array(
      '#type' => 'container',
      '#attributes' => array(
        'ng-blocks-app' => $app['app_name'],
        'id' => 'ng-blocks-app-' . $app['app_name'],
      ),
    );

    $content = (is_callable($app['callback'])) ? $app['callback']($app) : '';
    if (is_string($content)) {
      $render['content']['#markup'] = $content;
    }
    else {
      $render['content'] = $content;
    }

    // Add the necessary javascript / css files and settings.
    $ng_path = libraries_get_path('angular');
    $ng_blocks_path = drupal_get_path('module', 'ng_blocks');

    $render['#attached']['css'][] = $ng_path . '/angular.css';
    $render['#attached']['css'][] = $ng_blocks_path . '/css/ng_blocks.css';

    $render['#attached']['js'] = array(
      array(
        'data' => $ng_path . '/angular' . $min . '.js',
        'weight' => -10000,
      ),
      array(
        'data' => $ng_path . '/angular-route' . $min . '.js',
        'weight' => -9999,
      ),
      array(
        'data' => $ng_path . '/angular-sanitize' . $min . '.js',
        'weight' => -9998,
      ),
      array(
        'data' => $ng_blocks_path . '/js/ng_blocks.js',
        'weight' => 10000,
      ),
      array(
        'data' => $ng_blocks_path . '/js/filter.js',
        'weight' => 10000,
      ),
      array(
        'data' => $ng_blocks_path . '/js/tpls.js',
        'weight' => 10000,
      ),
    );
    foreach ((array) $app['js'] as $value) {
      $render['#attached']['js'][] = $value;
    }

    $block['content']["ng-{$app['app_name']}"] = $render;
  }

  return $block;
}

/**
 * Helper function which returns minified file extension if required.
 */
function _ng_blocks_minified() {
  return (variable_get('preprocess_js', FALSE)) ? '.min' : '';
}
